package com.maisuradze.database

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.maisuradze.database.storage.SportActivity

class MainActivity : AppCompatActivity() {

    private lateinit var mileageView: TextView
    private lateinit var swimmingView: TextView
    private lateinit var receivedCaloriesView: TextView

    private lateinit var mileageInput: EditText
    private lateinit var swimmingInput: EditText
    private lateinit var receivedCaloriesInput: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mileageView = findViewById(R.id.mileage)
        swimmingView = findViewById(R.id.swimming)
        receivedCaloriesView = findViewById(R.id.receivedCalories)

        mileageInput = findViewById(R.id.mileageInput)
        swimmingInput = findViewById(R.id.swimmingDistanceInput)
        receivedCaloriesInput = findViewById(R.id.receivedCaloriesInput)

        loadActivityStats()
    }

    fun onSave(view: View) {
        if (view is Button) {

            val newActivity = SportActivity(
                0,
                mileageInput.text.toString().toDoubleOrNull(),
                swimmingInput.text.toString().toDoubleOrNull(),
                receivedCaloriesInput.text.toString().toDoubleOrNull()
            )

            App.INSTANCE.DB.getDAO().insert(newActivity)

            loadActivityStats()
        }
    }

    private fun loadActivityStats() {
        val mileages: MutableList<Double> = arrayListOf()
        val swimmingDistances: MutableList<Double> = arrayListOf()
        val receivedCalories: MutableList<Double> = arrayListOf()

        App.INSTANCE.DB.getDAO().getAllSportActivities()?.forEach {
            it.mileage?.let { it1 -> mileages.add(it1) }
            it.swimming?.let { it1 -> swimmingDistances.add(it1) }
            it.receivedCalories?.let { it1 -> receivedCalories.add(it1) }
        }

        if (mileages.size > 0) {
            mileageView.text = "Mileage\n\t Average: ${mileages.average()}\n\t Sum: ${mileages.sum()}"
        }
        if (swimmingDistances.size > 0) {
            swimmingView.text = "Swimming Distance\n\t Average: ${swimmingDistances.average()}\n\t Sum: ${swimmingDistances.sum()}"
        }
        if (receivedCalories.size > 0) {
            receivedCaloriesView.text = "Received Calories\n\t Average: ${receivedCalories.average()}\n\t Sum: ${receivedCalories.sum()}"
        }
    }


}