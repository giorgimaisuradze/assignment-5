package com.maisuradze.database.storage

import androidx.room.*

@Dao
interface SportActivityDAO {

    @Query("SELECT * FROM SPORT_ACTIVITY")
    fun getAllSportActivities(): List<SportActivity>?

    @Query("SELECT * FROM SPORT_ACTIVITY s WHERE s.id = :id")
    fun getSportActivityById(id: Int): SportActivity?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(sportActivity: SportActivity)

}