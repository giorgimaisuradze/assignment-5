package com.maisuradze.database.storage

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "sport_activity")
data class SportActivity (

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Int,

    @ColumnInfo(name = "mileage")
    val mileage: Double?,

    @ColumnInfo(name = "swimming_distance")
    val swimming: Double?,

    @ColumnInfo(name = "received_calories")
    val receivedCalories: Double?

)