package com.maisuradze.database.storage

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [SportActivity::class], version = 1)
abstract class ApplicationDatabase: RoomDatabase() {

    abstract fun getDAO(): SportActivityDAO

}