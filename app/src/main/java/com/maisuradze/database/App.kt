package com.maisuradze.database

import android.app.Application
import androidx.room.Room
import com.maisuradze.database.storage.ApplicationDatabase

class App : Application() {

    lateinit var DB: ApplicationDatabase

    companion object {
        lateinit var INSTANCE: App
            private set
    }

    override fun onCreate() {
        super.onCreate()

        INSTANCE = this

        DB = Room.databaseBuilder(
            applicationContext,
            ApplicationDatabase::class.java,
            "APP_DATABASE"
        ).allowMainThreadQueries().build()
    }

}